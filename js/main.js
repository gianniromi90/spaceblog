document.addEventListener('scroll', ()=>{
    let bg= document.querySelector('.header')

    bg.style.backgroundSize =  ` 100%, 4px 4px, ${ 100 - window.pageYOffset / 10}%`


    let wrapperMenu = document.querySelector('.system')
    wrapperMenu.style.transform = `rotateZ(${window.pageYOffset / 10}deg)`
})


document.querySelectorAll('.btn-accent').forEach(el=>{
    el.addEventListener('click', ()=>{
        document.querySelector('#beep').play()
    })
})

document.querySelectorAll('#wrapper').forEach(el=>{
    el.addEventListener('click', ()=>{
        document.querySelector('#beep1').play()
    })
})



document.querySelector('.wrapperMenu').addEventListener('click', ()=>{
        document.querySelector('.leftSide').classList.toggle('activeLeft')
        document.querySelector('.rightSide').classList.toggle('activeRight')
    })


document.querySelector('#wrapper').addEventListener('click', ()=>{

    let user = document.querySelector('#user')
    let list = document.querySelector('#list')
    let settings = document.querySelector('#settings')
    let envelope = document.querySelector('#envelope')
    let phone = document.querySelector('#phone')
    let overlay = document.querySelector('.overlay')


    user.classList.toggle('active')
    list.classList.toggle('active')
    settings.classList.toggle('active')
    envelope.classList.toggle('active')
    phone.classList.toggle('active')
    overlay.classList.toggle('active')
})


fetch('https://api.spacexdata.com/v4/launches')
        .then(response => response.json())
        .then(data => {
            let articlesWrapper = document.querySelector('#articlesWrapper')

            let dataSorted = data.sort((a,b) => b.date_unix - a.date_unix)

            let filteredDetails = dataSorted.filter(el => el.details != null)

            let filteredMedia = filteredDetails.filter(el => el.links.flickr.original[0])

            filteredMedia.forEach(el => {
                
                let div = document.createElement('div')
                div.classList.add('row','mb-5','bg-border')
                div.innerHTML = 
                `
                <div class="col-12 col-lg-8">
                    <h3 class="text-main mb-5">${el.date_utc}</h3>
                    <p>${el.details}</p>
                    
                    </div>
                    <div class="col-12 col-lg-4">
                    <img src="${el.links.flickr.original[0]}" class="img-fluid img-crop d-block mx-auto ml-lg-auto" alt="">
                    </div>
                    <div class="col-12 text-center">
                    <button class="btn btn-accent px-4 text-white my-0">Go to news</button></div>
                ` 
                articlesWrapper.appendChild(div)
            });
           
        })